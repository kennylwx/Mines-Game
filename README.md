# Mines Game

- Task Instruction: <https://codesandbox.io/s/easygo-mines-test-snr62?file=/src/App.tsx>
- Game Example: <https://parmaday.club/casino/games/mines>

## Task Instruction

### Functional Requirements

#### Set Up

- The game is defined by a 5x5 grid with all of the tiles hidden at the start.

#### User Flow

1. Before the game starts, the user can set:
   a. the number of mines
   b. the betting amount
2. After setting the parameters of the game, the user can click on "BET", causing the game to start.
3. The user has the option to select the hidden tiles. After clicking on a hidden tile, an object is reaveled to either a Gem or a Mine.
   a. If the revealed object is a Gem, the game continues.
   b. If the revealed object is a Mine, the game ends.
4. While the game is still in progress (it has not ended), and the user has revealed at least 1 Gem, the user can click on "CASHOUT" to take their winnings.
   a. The Winnings is calculated by **the betting amount** multiplied by the **gems revealed by the user** and the **number of mines** set at the start of the game
5. When the game ends, the game restarts (back to #1).

### Non-functional Requirements

- Must use Gem image.
- Must use Mine image.
- Must use APIs from the api.ts file.
- Must use gem.mp3 and mine.mp3 when the relevant object is revelaed.
- Must make the game exciting, with focus on animations and user experience.

## Game Example Issues

### Issue 1: Confusing Start to the Game

#### Issue 1 Problem

It is not entirely clear that the user needs to click on "BET" to start the game. The presentation of the game does not build a clear path for the user to start the game as intended.

Presentation issues such as:

1. the emptiness of the tiles that spans more than 70% of the screen before the game starts,
2. the animation that occurs when hovering over the tiles before the start of the game,
3. the non-existent instructions, and
4. the vaguely named "Bet" button

#### Issue 1 Solution

The solution is to have a more explicit and descriptive instruction at the start of the game.

### Issue 2: Dual Purpose Left Column Section

#### Issue 2 Problem

The Left Column Panel is used by 2 conflicting purposes, with little to no in between breaks. The 2 Conflicting purposes are:

1. to initialise the game parameters
2. to notify the user the current game's parameters.

#### Issue 2 Solution

The solution is to have an explicit title, to inform the user what state they are in for both the Left Column Section and the Right Game Section.

### Issue 3: Unclear Ending

#### Issue 3 Problem

There are 3 types of ending to this game:

1. Mine Reveal Ending
2. Cashout Ending
3. All Gem Reveal Ending

In all of the endings, the Left Column Panel immediately goes back to the Game Setup state, while the Right Game Section only reveals all of the hidden objects. There is no explicit instruction stating that the game has ended.

This presentation does not confidently lead the user to restart and play the game, especially when the only indicator is a vague green-lit "BET" button.

#### Issue 3 Solution

The solution is to have a more dramatic effect, when you lose and win, to firmly establish the results of the game, and to ease the user to restart the game.
