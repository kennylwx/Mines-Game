import styled from "@emotion/styled";

const Container = styled.div`
  padding: 24px;
`;

const Title = styled.h1`
  font-size: 24px;
  font-weight: 500;
  margin: 0;
  padding: 0;
`;

const Body = styled.p`
  font-size: 16px;
  margin-top: 12px;
`;

const Footer = styled.div`
  font-size: 14px;
`;

export { Container, Title, Body, Footer };
