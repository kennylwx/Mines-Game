import React from "react";
import { Container, Footer, Body, Title } from "./styles";

const IntroSignage = () => {
  return (
    <Container>
      <Title>Mines Game: A Game of Luck and Skill</Title>
      <Body>
        Please enter the amount you would like to bet and select the number of
        mines you wish to have in your game. Click on the green <b>BET</b>{" "}
        button when you are ready to play. Good luck and have fun! &#128522;
      </Body>
      <Footer>
        Side Note: This game is not real, and you can't win any actual money.
      </Footer>
    </Container>
  );
};

export default IntroSignage;
