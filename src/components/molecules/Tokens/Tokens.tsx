import React from "react";
import { Label, Value, Container } from "./styles";
import { numberWithCommas } from "../../../@utils";

type TokensProps = {
  label: React.ReactNode;
  value: number;
};

const Tokens = ({ label, value }: TokensProps) => {
  return (
    <Container>
      <Label>{label}</Label>
      <Value>{numberWithCommas(value)}</Value>
    </Container>
  );
};

export default Tokens;
