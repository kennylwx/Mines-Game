import styled from "@emotion/styled";

const Container = styled.div`
  display: flex;
  align-items: center;

  border: 1px solid black;
  border-radius: 12px;
  overflow: hidden;

  height: 36px;
  width: 150px;
`;

const Label = styled.div`
  background-image: linear-gradient(
    45deg,
    hsl(124deg 100% 32%) 0%,
    hsl(100deg 100% 34%) 11%,
    hsl(91deg 100% 35%) 22%,
    hsl(84deg 100% 37%) 33%,
    hsl(78deg 100% 38%) 44%,
    hsl(73deg 100% 40%) 56%,
    hsl(68deg 100% 41%) 67%,
    hsl(63deg 100% 43%) 78%,
    hsl(59deg 100% 45%) 89%,
    hsl(55deg 98% 51%) 100%
  );
  display: flex;
  align-items: center;

  height: 100%;

  padding: 0 8px;
`;

const Value = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;

  height: 100%;
  width: 100%;

  padding: 0px 8px;
`;

export { Container, Label, Value };
