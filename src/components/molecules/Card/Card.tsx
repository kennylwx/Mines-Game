import React from "react";
import {
  CardBack,
  CardBackContent,
  CardFace,
  CardFaceContent,
  Container,
} from "./styles";

type CardProps = {
  onClick: (index: number) => void;
  isMine: boolean;
  card: React.ReactNode;
  index: number;
  isFlipped: boolean;
  isDisabled: boolean;
};

const Card = ({
  onClick,
  card,
  isMine,
  index,
  isFlipped,
  isDisabled,
}: CardProps) => {
  const handleClick = () => {
    !isFlipped && !isDisabled && onClick(index);
  };

  return (
    <Container
      onClick={handleClick}
      isFlipped={isFlipped}
      isDisabled={isDisabled || isFlipped}
    >
      <CardFace>
        <CardFaceContent />
      </CardFace>
      <CardBack>
        <CardBackContent isMine={isMine}>{card}</CardBackContent>
      </CardBack>
    </Container>
  );
};

export default Card;
