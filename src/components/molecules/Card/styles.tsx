import styled from "@emotion/styled";

const cardBgColor = "white";
const cardFaceContentColor = "#dcdcdc";
const cardBackContentColor = "white";

const mineColor = "#FD023E";
const gemColor = "#0FFD05";

type ContainerProps = {
  isFlipped: boolean;
  isDisabled: boolean;
};

const Container = styled.div<ContainerProps>`
  width: 100%;
  height: 100%;
  border-radius: 8px;
  box-shadow: rgba(0, 0, 0, 0.18) 0px 2px 4px;
  transition: 0.3s;
  transform-style: preserve-3d;
  position: relative;
  cursor: ${(props) => (props.isDisabled ? "default" : "pointer")};

  background-color: ${cardBgColor};

  ${(props) => (props.isFlipped ? `transform: rotateY(180deg);` : ``)}

  &:hover {
    ${(props) => (props.isDisabled ? "" : "top: -3px;")}
  }
`;

const CardFace = styled.div`
  backface-visibility: hidden;
  position: absolute;
  width: 100%;
  height: 100%;

  display: flex;
  justify-content: center;
  align-items: center;
`;

const CardFaceContent = styled.div`
  background-color: ${cardFaceContentColor};
  box-sizing: border-box;
  height: 90%;
  width: 90%;
  border-radius: 8px;
`;

const CardBack = styled.div`
  backface-visibility: hidden;
  position: absolute;
  width: 100%;
  height: 100%;

  transform: rotateY(180deg);

  display: flex;
  justify-content: center;
  align-items: center;
`;

type CardBackContentProps = {
  isMine: boolean;
};

const CardBackContent = styled.div<CardBackContentProps>`
  background-color: ${cardBackContentColor};

  height: 90%;
  width: 90%;

  border: 3px solid ${(props) => (props.isMine ? mineColor : gemColor)};
  border-radius: 5px;

  display: flex;
  justify-content: center;
  align-items: center;
`;

export { Container, CardFace, CardFaceContent, CardBack, CardBackContent };
