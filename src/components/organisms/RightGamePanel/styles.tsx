import styled from "@emotion/styled";

const sidePadding = 24;
const rightBackgroundColor = "#fdfdfd";

type ContainerProps = {
  row: number;
  col: number;
};

const Container = styled.div<ContainerProps>`
  background-color: ${rightBackgroundColor};
  flex: 1;

  display: flex;
  box-sizing: border-box;

  padding: 24px ${sidePadding}px 24px;

  display: grid;
  grid-template-columns: repeat(${(props) => props.col}, 1fr);
  grid-template-rows: repeat(${(props) => props.row}, 1fr);
  justify-items: center;
  align-items: stretch;
  gap: 1rem;

  perspective: 100%;
`;

const GameButton = styled.button`
  background-color: orange;
  height: 50px;
  width: 50px;
`;

export { Container, GameButton };
