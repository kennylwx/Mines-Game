import React, { Dispatch, SetStateAction, useState } from "react";
import useSound from "use-sound";
import gemSound from "../../../assets/sounds/gem.mp3";
import mineSound from "../../../assets/sounds/mine.mp3";
import { GAME_CYCLE_LOSS } from "../../../@constants";
import { useGameArray } from "../../../hooks";
import { Gem, Mine } from "../../atoms";
import { Card } from "../../molecules";
import { Container } from "./styles";
import { GameCycle } from "../../../@types";
import { isTargetSymbol } from "../../../@utils";
import { CasinoGameMines } from "../../../api";

const MINE_SYMBOL = "0";
const GEM_SYMBOL = "1";
const DURATION_TAKEN_TO_REVEAL_ALL_CARDS = 1 * 1000;

type RightGamePanelProps = {
  gameCol: number;
  gameRow: number;
  mines: number[];
  increaseNumOfGemsCollected: () => void;
  setCurrentGameCyle: (gameCycle: GameCycle) => void;
  disable: boolean;
  setDisable: Dispatch<SetStateAction<boolean>>;
  minesNext: (tileToReveal: number) => Promise<CasinoGameMines>;
};

// Adapted and inspired from
// https://codesandbox.io/s/clever-smoke-77b6s?from-embed=&file=/src/App.js:1230-1286
const RightGamePanel = ({
  gameCol,
  gameRow,
  mines,
  increaseNumOfGemsCollected,
  setCurrentGameCyle,
  disable,
  setDisable,
  minesNext,
}: RightGamePanelProps) => {
  const [playGemSound] = useSound(gemSound);
  const [playMineSound] = useSound(mineSound);

  const { cards } = useGameArray({
    gemSymbol: GEM_SYMBOL,
    mineSymbol: MINE_SYMBOL,
    totalCards: gameCol * gameRow,
    mines: mines,
  });

  const [openCards, setOpenCards] = useState<number[]>([]);

  const openAllCards = () => {
    // Create an array of indexes to the length of all cards
    const allCardsIndexes = Array.from(Array(gameCol * gameRow).keys());

    // Get the indexes of all the non opened cards
    // Adapted from https://stackoverflow.com/questions/1187518/how-to-get-the-difference-between-two-arrays-in-javascript
    const notOpenedCardsIndexes = allCardsIndexes
      .filter((x) => !openCards.includes(x))
      .concat(openCards.filter((x) => !allCardsIndexes.includes(x)));

    // Open the rest of the unopened cards
    setOpenCards((prev) => [...prev.concat(notOpenedCardsIndexes)]);
  };

  const evaluateCard = (index: number) => {
    if (isTargetSymbol(MINE_SYMBOL, cards[index])) {
      // Play Mine Sound Effect
      playMineSound();

      // Push the Game Lifecycle to Loss
      setCurrentGameCyle(GAME_CYCLE_LOSS);

      // Open all the cards
      setTimeout(function () {
        openAllCards();
      }, DURATION_TAKEN_TO_REVEAL_ALL_CARDS);
    } else if (isTargetSymbol(GEM_SYMBOL, cards[index])) {
      // Play Gem sound effect
      playGemSound();

      // Increase number of gems collected
      increaseNumOfGemsCollected();
    } else {
      console.log("Event: Unknown");
    }
  };

  // Handle Card Click
  const handleCardClick = (index: number) => {
    setDisable(true);
    minesNext(index).then((res) => {
      setDisable(false);
      // Open card that was clicked on
      setOpenCards((prev) => [...prev, index]);

      // Evaluate the card that was clicked on
      evaluateCard(index);
    });
  };

  const checkIsFlipped = (index: number) => {
    return openCards.includes(index);
  };

  return (
    <Container row={gameRow} col={gameCol}>
      {cards.map((cardVal, index) => {
        return (
          <Card
            key={`card-${index}`}
            isMine={isTargetSymbol(MINE_SYMBOL, cardVal)}
            card={
              isTargetSymbol(MINE_SYMBOL, cardVal) ? (
                <Mine height={42} width={42} />
              ) : (
                <Gem height={42} width={42} />
              )
            }
            index={index}
            isDisabled={disable}
            isFlipped={checkIsFlipped(index)}
            onClick={handleCardClick}
          />
        );
      })}
    </Container>
  );
};

export default RightGamePanel;
