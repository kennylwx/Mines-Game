import React from "react";
import { GiMoneyStack } from "react-icons/gi";
import { Container, Title } from "./styles";
import { Tokens } from "../../molecules";

type HeaderProps = {
  title: string;
  tokenValue: number;
};

const Header = ({ title, tokenValue }: HeaderProps) => {
  return (
    <Container height={"60px"}>
      <Title>{title}</Title>
      {/* <Tokens value={tokenValue} label={<GiMoneyStack size={24} />} /> */}
    </Container>
  );
};

export default Header;
