import styled from "@emotion/styled";

const sidePadding = 24;
const headerBackgroundColor = "white";

type ContainerProps = {
  height: string;
};

const Container = styled.div<ContainerProps>`
  background-color: ${headerBackgroundColor};
  width: 100%;
  height: ${(props) => props.height};

  padding: 0px ${sidePadding}px;

  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;

  border-bottom: 1px solid #dcdcdc;
`;

const Title = styled.h1`
  text-transform: uppercase;
  font-weight: 700;
  font-size: 1.4em;
  margin: 0;
  padding: 0;
`;

export { Container, Title };
