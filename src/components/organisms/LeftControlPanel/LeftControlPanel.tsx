import React from "react";
import { BsCash } from "react-icons/bs";
import {
  GAME_CYCLE_BET,
  GAME_CYCLE_CHECKOUT,
  GAME_CYCLE_INIT,
  GAME_CYCLE_LOSS,
  GAME_CYCLE_WIN,
} from "../../../@constants";
import { GameCycle } from "../../../@types";
import { numberWithCommas } from "../../../@utils";
import {
  Container,
  BetAmountContainer,
  BetAmountInput,
  NumOfMinesContainer,
  NumOfMinesInput,
  ButtonContainer,
  Button,
  PropertyContainer,
  PropertyLabel,
  PropertyValue,
  EndingMessageTitle,
  EndingMessageBody,
  EndingMessage,
} from "./styles";

type InitControlsProps = {
  betAmountLabel: string;
  minBetAmount: number;
  maxBetAmount: number;
  betAmountValue: number;
  setBetAmountValue: (betAmount: number) => void;
  numOfMinesLabel: string;
  numOfMinesData: {
    value: string;
    label: string;
  }[];
  numOfMinesValue: number;
  onBet: () => void;
  onSelectChange: (val: string | null) => void[];
  onLoading: boolean;
};

const InitControls = ({
  maxBetAmount,
  minBetAmount,
  betAmountLabel,
  betAmountValue,
  setBetAmountValue,
  numOfMinesLabel,
  numOfMinesData,
  numOfMinesValue,
  onSelectChange,
  onBet,
  onLoading,
}: InitControlsProps) => {
  return (
    <Container>
      <BetAmountContainer>
        <BetAmountInput
          max={maxBetAmount}
          min={minBetAmount}
          label={betAmountLabel}
          value={betAmountValue}
          onChange={setBetAmountValue}
          hideControls
          withAsterisk
          disabled={onLoading}
        />
      </BetAmountContainer>

      <NumOfMinesContainer>
        <NumOfMinesInput
          withAsterisk
          label={numOfMinesLabel}
          data={numOfMinesData}
          value={numOfMinesValue.toString()}
          onChange={onSelectChange}
          disabled={onLoading}
        />
      </NumOfMinesContainer>

      <ButtonContainer>
        <Button
          variant="gradient"
          leftIcon={<BsCash />}
          gradient={{ from: "teal", to: "lime", deg: 105 }}
          type="button"
          fullWidth
          uppercase
          onClick={onBet}
          loading={onLoading}
        >
          Bet
        </Button>
      </ButtonContainer>
    </Container>
  );
};

type BetControlsProps = {
  betAmountLabel: string;
  betAmountValue: number;
  numOfMinesLabel: string;
  numOfMinesValue: number;
  onCheckout: () => void;
  numOfGemsCollected: number;
  numOfGemsCollectedLabel: string;
  onLoading: boolean;
};

const BetControls = ({
  betAmountLabel,
  betAmountValue,
  numOfMinesLabel,
  numOfMinesValue,
  onCheckout,
  numOfGemsCollected,
  numOfGemsCollectedLabel,
  onLoading,
}: BetControlsProps) => {
  return (
    <Container>
      <PropertyContainer>
        <PropertyLabel>{betAmountLabel}</PropertyLabel>
        <PropertyValue>{`$${numberWithCommas(betAmountValue)}`}</PropertyValue>
      </PropertyContainer>

      <PropertyContainer>
        <PropertyLabel>{numOfMinesLabel}</PropertyLabel>
        <PropertyValue>{numOfMinesValue}</PropertyValue>
      </PropertyContainer>

      <PropertyContainer>
        <PropertyLabel>{numOfGemsCollectedLabel}</PropertyLabel>
        <PropertyValue>{numOfGemsCollected}</PropertyValue>
      </PropertyContainer>

      <ButtonContainer>
        <Button
          variant="gradient"
          leftIcon={<BsCash />}
          gradient={{ from: "teal", to: "lime", deg: 105 }}
          type="button"
          fullWidth
          uppercase
          onClick={onCheckout}
          disabled={numOfGemsCollected === 0}
          loading={onLoading}
        >
          Checkout
        </Button>
      </ButtonContainer>
    </Container>
  );
};

type EndControlsProps = {
  betAmountValue: number;
  numOfMinesValue: number;
  onBetAgain: () => void;
  currentGameCycle: GameCycle;
  numOfGemsCollected: number;
};

const EndControls = ({
  betAmountValue,
  numOfMinesValue,
  onBetAgain,
  currentGameCycle,
  numOfGemsCollected,
}: EndControlsProps) => {
  // Winnings from collecting gems and checking out before hitting a mine
  const checkoutWinnings = numberWithCommas(
    betAmountValue + (numOfMinesValue * numOfGemsCollected) / 100
  );

  // Winnings for collecting all of the gems
  const jackpotWinnings = numberWithCommas(betAmountValue * numOfMinesValue);

  const displayProperties = () => {
    switch (currentGameCycle) {
      case GAME_CYCLE_LOSS:
        return (
          <EndingMessage isSuccess={false}>
            <EndingMessageTitle>
              You've Hit A Mine! &#128542;
            </EndingMessageTitle>
            <EndingMessageBody>
              Unfortunately, you did not win anything this time around.
            </EndingMessageBody>
          </EndingMessage>
        );

      case GAME_CYCLE_CHECKOUT:
        return (
          <EndingMessage isSuccess={true}>
            <EndingMessageTitle>
              Congrats! &#127881; &#127881; &#127881;
            </EndingMessageTitle>
            <EndingMessageBody>
              For collecting {numOfGemsCollected} gem(s), you have won a total
              of $ {checkoutWinnings}
            </EndingMessageBody>
          </EndingMessage>
        );

      case GAME_CYCLE_WIN:
        return (
          <EndingMessage isSuccess={true}>
            <EndingMessageTitle>
              Jackpot! &#129395; &#129395; &#129395;
            </EndingMessageTitle>
            <EndingMessageBody>
              For collecting all of the gems, you have won a total of $
              {jackpotWinnings}
            </EndingMessageBody>
          </EndingMessage>
        );

      default:
        return <></>;
    }
  };

  return (
    <Container>
      {displayProperties()}

      <ButtonContainer>
        <Button
          variant="gradient"
          leftIcon={<BsCash />}
          gradient={{ from: "teal", to: "lime", deg: 105 }}
          type="button"
          fullWidth
          uppercase
          onClick={onBetAgain}
        >
          Play Again
        </Button>
      </ButtonContainer>
    </Container>
  );
};

type LeftControlPanelProps = {
  betAmountLabel: string;
  minBetAmount: number;
  maxBetAmount: number;
  betAmountValue: number;
  setBetAmountValue: (betAmount: number) => void;
  numOfMinesLabel: string;
  numOfMinesData: {
    value: string;
    label: string;
  }[];
  numOfMinesValue: number;
  setNumOfMinesValue: (numOfMines: number) => void;
  onBet: () => void;
  onCheckout: () => void;
  onBetAgain: () => void;
  currentGameCycle: string;
  numOfGemsCollected: number;
  numOfGemsCollectedLabel: string;
  onLoading: boolean;
};

const LeftControlPanel = ({
  betAmountLabel,
  minBetAmount,
  maxBetAmount,
  betAmountValue,
  setBetAmountValue,
  numOfMinesLabel,
  numOfMinesData,
  numOfMinesValue,
  setNumOfMinesValue,
  onBet,
  onCheckout,
  onBetAgain,
  currentGameCycle,
  numOfGemsCollected,
  numOfGemsCollectedLabel,
  onLoading,
}: LeftControlPanelProps) => {
  const onSelectChange = (val: string | null) => [
    setNumOfMinesValue(val ? parseInt(val) : 1),
  ];

  switch (currentGameCycle) {
    case GAME_CYCLE_INIT:
      return (
        <InitControls
          maxBetAmount={maxBetAmount}
          minBetAmount={minBetAmount}
          betAmountLabel={betAmountLabel}
          betAmountValue={betAmountValue}
          setBetAmountValue={setBetAmountValue}
          numOfMinesLabel={numOfMinesLabel}
          numOfMinesData={numOfMinesData}
          numOfMinesValue={numOfMinesValue}
          onBet={onBet}
          onSelectChange={onSelectChange}
          onLoading={onLoading}
        />
      );
    case GAME_CYCLE_BET:
      return (
        <BetControls
          betAmountLabel={betAmountLabel}
          betAmountValue={betAmountValue}
          numOfMinesLabel={numOfMinesLabel}
          numOfMinesValue={numOfMinesValue}
          onCheckout={onCheckout}
          numOfGemsCollected={numOfGemsCollected}
          numOfGemsCollectedLabel={numOfGemsCollectedLabel}
          onLoading={onLoading}
        />
      );

    case GAME_CYCLE_LOSS:
      return (
        <EndControls
          betAmountValue={betAmountValue}
          numOfMinesValue={numOfMinesValue}
          onBetAgain={onBetAgain}
          currentGameCycle={currentGameCycle}
          numOfGemsCollected={numOfGemsCollected}
        />
      );

    case GAME_CYCLE_CHECKOUT:
      return (
        <EndControls
          betAmountValue={betAmountValue}
          numOfMinesValue={numOfMinesValue}
          onBetAgain={onBetAgain}
          currentGameCycle={currentGameCycle}
          numOfGemsCollected={numOfGemsCollected}
        />
      );

    case GAME_CYCLE_WIN:
      return (
        <EndControls
          betAmountValue={betAmountValue}
          numOfMinesValue={numOfMinesValue}
          onBetAgain={onBetAgain}
          currentGameCycle={currentGameCycle}
          numOfGemsCollected={numOfGemsCollected}
        />
      );

    default:
      return <></>;
  }
};

export default LeftControlPanel;
