import styled from "@emotion/styled";
import { Button, NumberInput, Select } from "@mantine/core";

const leftWidth = "250px";
const sidePadding = 24;
const topPadding = 24;
const leftBackgroundColor = "white";

const Container = styled.div`
  background-color: ${leftBackgroundColor};

  width: ${leftWidth};
  min-width: ${leftWidth};
  max-width: ${leftWidth};

  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;

  box-sizing: border-box;

  padding: 0px ${sidePadding}px;

  border-right: 1px solid #dcdcdc;
`;

const BetAmountContainer = styled.div`
  margin-top: ${topPadding}px;
  width: 100%;

  display: flex;
`;

const BetAmountInput = styled(NumberInput)`
  width: 100%;
`;

const NumOfMinesContainer = styled.div`
  margin-top: ${topPadding}px;
  width: 100%;
`;

const NumOfMinesInput = styled(Select)`
  &:disabled {
    background-color: orange;
  }
`;

const NumOfGemsCollectedContainer = styled.div`
  margin-top: ${topPadding}px;
  width: 100%;

  display: flex;
`;
const NumOfGemsCollectedInput = styled(NumberInput)`
  width: 100%;
`;

const PropertyContainer = styled.div`
  margin-top: ${topPadding}px;
  width: 100%;

  display: flex;
  flex-direction: column;
`;

const PropertyLabel = styled.div`
  font-size: 14px;
  font-weight: 500;
`;

const PropertyValue = styled.div`
  font-size: 14px;

  border: 1px solid #ced4da;
  border-radius: 2px;

  height: 36px;

  padding: 1px 12px;

  display: flex;
  align-items: center;
`;

type EndingMessageProps = {
  isSuccess: boolean;
};

const EndingMessage = styled.div<EndingMessageProps>`
  margin-top: ${topPadding}px;

  border: 2.5px solid ${(props) => (props.isSuccess ? "#0FFD05" : "#FD023E")};
  border-radius: 2px;

  padding: 8px 12px;

  display: flex;
  flex-direction: column;
`;

const EndingMessageTitle = styled.div`
  font-weight: 600;
  font-size: 16px;
`;

const EndingMessageBody = styled.div`
  margin-top: 12px;
  font-weight: 400;
  font-size: 14px;
`;

const ButtonContainer = styled.div`
  margin-top: ${topPadding}px;
  width: 100%;
`;

export {
  Container,
  BetAmountContainer,
  BetAmountInput,
  NumOfMinesContainer,
  NumOfMinesInput,
  NumOfGemsCollectedContainer,
  NumOfGemsCollectedInput,
  PropertyContainer,
  PropertyLabel,
  PropertyValue,
  EndingMessage,
  EndingMessageTitle,
  EndingMessageBody,
  ButtonContainer,
  Button,
};
