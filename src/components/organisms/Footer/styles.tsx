import styled from "@emotion/styled";

const footerBackgroundColor = "white";

const Container = styled.div`
  background-color: ${footerBackgroundColor};
  height: 32px;
  width: 100%;

  display: flex;
  justify-content: center;
  align-items: center;

  border-top: 1px solid #dcdcdc;
`;

export { Container };
