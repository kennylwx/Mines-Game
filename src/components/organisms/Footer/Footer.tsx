import React from "react";
import { Container } from "./styles";

type FooterProps = {
  label: string;
};

const Footer = ({ label }: FooterProps) => {
  return <Container>{label}</Container>;
};

export default Footer;
