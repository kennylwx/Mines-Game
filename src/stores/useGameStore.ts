import { create } from "zustand";
import { GAME_CYCLE_INIT } from "../@constants";
import { GameCycle } from "../@types";

interface GameState {
  // Number of Gems Collected Before Cashing out or Loss
  numOfGemsCollected: number;
  increaseNumOfGemsCollected: () => void;
  resetNumOfGemsCollected: () => void;

  // Game Parameters
  gameRow: number;
  setGameRow: (gameRow: number) => void;
  gameCol: number;
  setGameCol: (gameCol: number) => void;
  numOfMines: number;
  setNumOfMines: (numOfMines: number) => void;
  maxNumOfMines: number;
  setMaxNumOfMines: (maxNumOfMines: number) => void;
  betAmount: number;
  setBetAmount: (betAmount: number) => void;
  minesLocation: number[];
  setMinesLocation: (minesLocation: number[]) => void;

  // Game Life Cycle
  currentGameCycle: GameCycle;
  setCurrentGameCyle: (cycle: GameCycle) => void;
}

const startingGemsCollected = 0;
const defaultGameRow = 5;
const defaultGameCol = 5;
const defaultBetAmount = 1000;
const defaultNumOfMines = 2;
const maxNumOfMines = 5;

const useGameStore = create<GameState>((set) => ({
  numOfGemsCollected: startingGemsCollected,
  increaseNumOfGemsCollected: () =>
    set((state) => ({
      ...state,
      numOfGemsCollected: state.numOfGemsCollected + 1,
    })),
  resetNumOfGemsCollected: () =>
    set((state) => ({
      ...state,
      numOfGemsCollected: 0,
    })),
  gameRow: defaultGameRow,
  setGameRow: (gameRow) =>
    set((state) => ({
      ...state,
      gameRow,
    })),

  gameCol: defaultGameCol,
  setGameCol: (gameCol) =>
    set((state) => ({
      ...state,
      gameCol,
    })),

  numOfMines: defaultNumOfMines,
  setNumOfMines: (numOfMines) =>
    set((state) => ({
      ...state,
      numOfMines,
    })),

  maxNumOfMines: maxNumOfMines,
  setMaxNumOfMines: (maxNumOfMines) =>
    set((state) => ({
      ...state,
      maxNumOfMines,
    })),

  betAmount: defaultBetAmount,
  setBetAmount: (betAmount) =>
    set((state) => ({
      ...state,
      betAmount,
    })),

  minesLocation: [],
  setMinesLocation: (minesLocation) =>
    set((state) => ({
      ...state,
      minesLocation,
    })),

  currentGameCycle: GAME_CYCLE_INIT,
  setCurrentGameCyle: (cycle) =>
    set((state) => ({
      ...state,
      currentGameCycle: cycle,
    })),
}));

export default useGameStore;
