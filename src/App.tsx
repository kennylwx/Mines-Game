import React from "react";
import { Route } from "wouter";
import { HomePage } from "./pages";
import { MantineProvider } from "@mantine/core";

const App = () => {
  return (
    <MantineProvider withGlobalStyles withNormalizeCSS>
      <Route path="/" component={HomePage} />
    </MantineProvider>
  );
};

export default App;
