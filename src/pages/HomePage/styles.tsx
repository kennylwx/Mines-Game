import styled from "@emotion/styled";

const Container = styled.div`
  display: flex;
  height: 100%;
  width: 100%;
`;

export { Container };
