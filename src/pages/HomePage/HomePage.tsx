import React, { useEffect, useState } from "react";
import Confetti from "react-confetti";
import {
  GAME_CYCLE_BET,
  GAME_CYCLE_CHECKOUT,
  GAME_CYCLE_INIT,
  GAME_CYCLE_LOSS,
  GAME_CYCLE_WIN,
} from "../../@constants";
import {
  Footer,
  Header,
  IntroSignage,
  LeftControlPanel,
  RightGamePanel,
} from "../../components";
import { HomeLayout } from "../../layouts";
import { useGameStore } from "../../stores";
import { Container } from "./styles";
import { useWindowSize } from "../../hooks";
import { minesBet, minesCashout, minesNext } from "../../api";

const HomePage = () => {
  const {
    numOfMines,
    setNumOfMines,
    maxNumOfMines,
    betAmount,
    setBetAmount,
    currentGameCycle,
    setCurrentGameCyle,
    gameCol,
    gameRow,
    numOfGemsCollected,
    increaseNumOfGemsCollected,
    resetNumOfGemsCollected,
    minesLocation,
    setMinesLocation,
  } = useGameStore();

  // Header Properties
  const headerTitle = "Mines Game";

  // User Token Properties
  const defaultTokenValue = 1000000;
  const [tokenValue, setTokenValue] = useState<number>(defaultTokenValue);

  // Footer Properties
  const footerLabel = "Made by Kenny Lee";

  // Bet Amount Properties
  const minBetAmount = 1;
  const maxBetAmount = 1000000;
  const betAmountLabel = "Bet Amount";

  // Num of Mines Properties
  const numOfMinesLabel = "Number of Mines";
  const numOfMinesData = Array(maxNumOfMines)
    .fill(0)
    .map((_, i) => {
      return { value: `${i + 1}`, label: `${i + 1}` };
    });

  // Gems Collected Label
  const numOfGemsCollectedLabel = "Gems Collected";

  const [isGameLoading, setIsGameLoading] = useState<boolean>();

  // Handle when user clicks on BET button
  const handleBet = () => {
    setIsGameLoading(true);
    minesBet(numOfMines).then((res) => {
      // Set the Mines Location
      setMinesLocation(res.mines);
      setCurrentGameCyle(GAME_CYCLE_BET);
      setIsGameLoading(false);
    });
  };

  // Handle when user clicks on CHECKOUT  button
  const handleCheckout = () => {
    minesCashout().then(() => {
      // Checkout with winnings
      setCurrentGameCyle(GAME_CYCLE_CHECKOUT);
    });
  };

  const handleBetAgain = () => {
    setCurrentGameCyle(GAME_CYCLE_INIT);
  };

  // Disable all cards
  const [disableAllCards, setDisableAllCards] = useState(false);
  const disable = () => {
    setDisableAllCards(true);
  };

  const enable = () => {
    setDisableAllCards(false);
  };

  // Initialise different game state with various properties
  useEffect(() => {
    switch (currentGameCycle) {
      case GAME_CYCLE_INIT:
        resetNumOfGemsCollected();
        enable();
        break;
      case GAME_CYCLE_BET:
        resetNumOfGemsCollected();
        enable();
        break;
      case GAME_CYCLE_CHECKOUT:
        disable();
        break;
      case GAME_CYCLE_LOSS:
        disable();
        break;
      case GAME_CYCLE_WIN:
        disable();
        break;
      default:
        break;
    }
  }, [currentGameCycle]);

  // Check for winning condition (Get All Gems)
  useEffect(() => {
    const totalCards = gameRow * gameCol;
    const totalAvailableGems = totalCards - numOfMines;

    if (numOfGemsCollected === totalAvailableGems) {
      setCurrentGameCyle(GAME_CYCLE_WIN);
    }
  }, [numOfGemsCollected]);

  const { width, height } = useWindowSize();

  return (
    <>
      {currentGameCycle === GAME_CYCLE_WIN ? (
        <Confetti width={width} height={height} />
      ) : (
        <></>
      )}
      <HomeLayout
        header={<Header title={headerTitle} tokenValue={tokenValue} />}
        body={
          <Container>
            <LeftControlPanel
              betAmountLabel={betAmountLabel}
              minBetAmount={minBetAmount}
              maxBetAmount={maxBetAmount}
              betAmountValue={betAmount}
              setBetAmountValue={setBetAmount}
              numOfMinesLabel={numOfMinesLabel}
              numOfMinesData={numOfMinesData}
              numOfMinesValue={numOfMines}
              setNumOfMinesValue={setNumOfMines}
              onBet={handleBet}
              onCheckout={handleCheckout}
              onBetAgain={handleBetAgain}
              currentGameCycle={currentGameCycle}
              numOfGemsCollected={numOfGemsCollected}
              numOfGemsCollectedLabel={numOfGemsCollectedLabel}
              onLoading={isGameLoading || false}
            />

            {currentGameCycle !== GAME_CYCLE_INIT ? (
              <RightGamePanel
                gameCol={gameCol}
                gameRow={gameRow}
                mines={minesLocation}
                increaseNumOfGemsCollected={increaseNumOfGemsCollected}
                setCurrentGameCyle={setCurrentGameCyle}
                disable={disableAllCards}
                setDisable={setDisableAllCards}
                minesNext={minesNext}
              />
            ) : (
              <>
                <IntroSignage />
              </>
            )}
          </Container>
        }
        footer={<Footer label={footerLabel} />}
      />
    </>
  );
};

export default HomePage;
