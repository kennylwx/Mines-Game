import styled from "@emotion/styled";

const Container = styled.div`
  height: 100vh;
  width: 100%;

  display: flex;
  flex-direction: column;
`;

const Header = styled.div`
  display: flex;
`;

const Body = styled.div`
  display: flex;
  flex: 1;
`;

const Footer = styled.div`
  display: flex;
`;

type SectionProps = {
  width: string;
  backgroundColor: string;
};

const Section = styled.div<SectionProps>`
  background-color: ${(props) => props.backgroundColor};
  width: ${(props) => props.width};
`;

export { Container, Header, Body, Footer, Section };
