import React from "react";
import { Body, Container, Footer, Header, Section } from "./styles";

type GameLayoutProps = {
  header: React.ReactNode;
  body: React.ReactNode;
  footer: React.ReactNode;
};

const GameLayout = ({ header, body, footer }: GameLayoutProps) => {
  return (
    <Container>
      <Header>{header}</Header>
      <Body>{body}</Body>
      <Footer>{footer}</Footer>
    </Container>
  );
};

export default GameLayout;
